/*
 * Copyright 2017 StreamSets Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package za.co.absa.stage.origin.sample;

import za.co.absa.stage.lib.sample.Errors;
import com.streamsets.pipeline.api.BatchMaker;
import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.api.base.BaseSource;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.streamsets.pipeline.api.Record;

/**
 * This source is an example and does not actually read from anywhere.
 * It does however, generate generate a simple record with one field.
 */
public abstract class SampleSource extends BaseSource {

  private final static Logger logger = Logger.getLogger("SampleSource");

  private final String FIELDS_SEPARATOR = "<<>>";
  private final String KEY_VALUE_SEPARATOR = ":";

  /**
   * Gives access to the UI configuration of the stage provided by the {@link SampleDSource} class.
   */
  public abstract String getTransactionsFile();

  private List<String> allTransactions = new ArrayList<String>();
  private int recordId = 0;

  @Override
  protected List<ConfigIssue> init() {
    // Validate configuration values and open any required resources.
    List<ConfigIssue> issues = super.init();


    File transactions = new File(getTransactionsFile());


    if (!transactions.exists()) {
      issues.add(
              getContext().createConfigIssue(
                      Groups.TRANSACTIONS.name(), "config", Errors.SAMPLE_00, "specified file does not exist"
              )
      );
    }

    if (!transactions.isFile()) {
      issues.add(
              getContext().createConfigIssue(
                      Groups.TRANSACTIONS.name(), "config", Errors.SAMPLE_00, "no file is invalid"
              )
      );
    }

    try {
      logger.info("Loading transactions from "+transactions.getAbsolutePath());
      allTransactions = Files.readAllLines(transactions.toPath());
      logger.info("Loaded "+allTransactions.size()+" transactions.");
    }
    catch (Exception e) {
      issues.add(
              getContext().createConfigIssue(
                      Groups.TRANSACTIONS.name(), "config", Errors.SAMPLE_00, "could not load transactions: "+e.getMessage()
              )
      );
      e.printStackTrace();
    }

    logger.info("All right.");
    System.out.println("All right");

    // If issues is not empty, the UI will inform the user of each configuration issue in the list.
    return issues;
  }

  /** {@inheritDoc} */
  @Override
  public void destroy() {
    // Clean up any open resources.
    allTransactions.clear();
    allTransactions = null;

    super.destroy();
  }

  /** {@inheritDoc} */
  @Override
  public String produce(String lastSourceOffset, int maxBatchSize, BatchMaker batchMaker) throws StageException {
    // Offsets can vary depending on the data source. Here we use an integer as an example only.
    int nextSourceOffset = 0;
    if (lastSourceOffset != null) {
      nextSourceOffset = Integer.parseInt(lastSourceOffset);
    }

    int endingOffset = nextSourceOffset + maxBatchSize;
    if (endingOffset > allTransactions.size()) {
      endingOffset = allTransactions.size();
    }

    // TODO: As the developer, implement your logic that reads from a data source in this method.
    List<String> batchTransactions = allTransactions.subList(nextSourceOffset, endingOffset);

    for (String transaction : batchTransactions) {
      logger.info("Processing transaction: "+transaction);
      System.out.println(transaction);
      Record record = createRecord(transactionStringToMap(transaction));
      batchMaker.addRecord(record);
    }

    return String.valueOf(endingOffset);

  }

  private final Record createRecord(Map<String,Field> fields) {
    Record record = getContext().createRecord("id: " + recordId++);
    record.set(Field.create(fields));
    return record;
  }

  private final Map<String,Field> transactionStringToMap(String transaction) {

    Map<String,Field> transactionFields = new  HashMap<String,Field>();

    String[] fields = transaction.split(FIELDS_SEPARATOR);

    for (String field : fields) {

      String[] items = field.split(KEY_VALUE_SEPARATOR);
      transactionFields.put(items[0].trim(), getField(items[0], items[1]));
    }

    return transactionFields;
  }

  private final Field getField(String key, String value) {

    switch (key) {

      case "name":
      case "id":
      case "destination":
      case "token": return Field.create(value);
      case "approved": return Field.create(Boolean.parseBoolean(value));
      case "amount": return Field.create(Double.parseDouble(value));
      default: return null;

    }
  }
}